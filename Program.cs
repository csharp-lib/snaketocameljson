﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Dynamic;
using System.IO;
using System.Net.Http;
using System.Net;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Serialization;

namespace snakeToCamelCase
{
    class Program
    {
        private static string Token = "KeyHxw3hpqG8a4s2m9cpaXfY8PMW2E";

        static async Task Main(string[] args)
        {
            Console.WriteLine("The snake to camel case is started here");

            // var responseBody = await GetResponse();
            // var res = await responseBody.Content.ReadAsAsync<object>();
            //

            var jsonString = await File.ReadAllTextAsync("json_sample_00.json");

            // using (var r = new StreamReader(Path. "/json_sample_00.json"))
            // {
            //     jsonString = await r.ReadToEndAsync();
            // }

            // var rss = JObject.Parse(jsonString);


            // foreach (JToken level0 in rss.Children())
            // {
            //     var prop0 = level0 as JProperty;
            //     Console.WriteLine("----- Level00 -----");
            //     Console.WriteLine("Level0: " + prop0.Name);
            //     Console.WriteLine("Level0: " + prop0.Type);

            //     foreach (JToken level1 in level0)
            //     {
            //         var prop1 = level1 as JProperty;
            //         Console.WriteLine("Level11:" + level1.Type);
            //         if (prop1 != null)
            //         {
            //             Console.WriteLine("----- Level01 -----");
            //             Console.WriteLine("Level1: " + prop1.Name);
            //             Console.WriteLine("Level1: " + prop1.Type);
            //         }

            //         foreach (JToken level2 in level1)
            //         {
            //             var prop2 = level2 as JProperty;
            //             if (prop2 != null)
            //             {
            //                 Console.WriteLine("----- Level02 -----");
            //                 Console.WriteLine("Level2: " + prop2.Name);
            //                 Console.WriteLine("Level2: " + prop2.Type);
            //             }

            //             foreach (JToken level3 in level2)
            //             {
            //                 var prop3 = level3 as JProperty;
            //                 if (prop3 != null)
            //                 {
            //                     Console.WriteLine("----- Level03 -----");
            //                     Console.WriteLine("Level3: " + prop3.Name);
            //                 }

            //                 foreach (JToken level4 in level3)
            //                 {
            //                     var prop4 = level4 as JProperty;
            //                     if (prop4 != null)
            //                     {
            //                         Console.WriteLine("----- Level04 -----");
            //                         Console.WriteLine("Level4: " + prop4.Name);
            //                     }

            //                     foreach (JToken level5 in level4)
            //                     {
            //                         var prop5 = level5 as JProperty;
            //                         if (prop5 != null)
            //                         {
            //                             Console.WriteLine("----- Level05 -----");
            //                             Console.WriteLine("Level5: " + prop5.Name);
            //                         }

            //                         foreach (JToken level6 in level5)
            //                         {
            //                             var prop6 = level6 as JProperty;
            //                             if (prop6 != null)
            //                             {
            //                                 Console.WriteLine("----- Level06 -----");
            //                                 Console.WriteLine("Level6: " + prop6.Name);
            //                             }
            //                         }
            //                     }
            //                 }
            //             }
            //         }

            //     }
            // }


            // var dic = new Dictionary<string, dynamic>();
            //
            // dic.Add("firstArg01", "Patompong Sulsaksakul");
            // dic.Add("tags", new List<string>
            // {
            //     "football",
            //     "bastketball"
            // });
            //
            //
            // Console.Write(JsonConvert.SerializeObject(dic));

            ConvertToCamelCase(JsonConvert.DeserializeObject(jsonString));
        }

        static void ConvertToCamelCase(object response)
        {
            // var tempDic = new Dictionary<string, dynamic>();

            var jsonString = JsonConvert.SerializeObject(response);
            JObject o = JObject.Parse(jsonString);
            JObject p = JObject.Parse(jsonString);


            var a = GetAllProperties(o);
            Console.Write("TEMP_DIC_PRINTED" + JsonConvert.SerializeObject(a));

            // Console.WriteLine("Full object: " + p);

            // foreach (JToken child in o.Children())
            // {
            //     // var curr = child as JProperty;
            //     // o.Property(curr.Name).Rename("BGH" + curr.Name);
            //     // var newProp = new JProperty("BGH" + curr.Name, curr.Name);
            //     // curr.Replace(newProp);
            //     // Console.WriteLine("Child: " + child);


            //     foreach (JToken grandChild in child)
            //     {
            //         // Console.WriteLine("GrandChild: " + grandChild);
            //         // Console.WriteLine("Type" + grandChild.Type);
            //         foreach (JToken grandGrandChild in grandChild)
            //         {
            //             var property = grandGrandChild as JProperty;

            //             if (property != null)
            //             {
            //                 // Console.WriteLine(property.Name + ":" + property.Value);
            //             }
            //         }
            //     }
            // }
        }

        private static Dictionary<string, dynamic> GetAllProperties(JToken children)
        {
            int level = 0;
            var tempDic = new Dictionary<string, dynamic>();

            foreach (JToken child in children.Children())
            {
                var property = child as JProperty;

                // if (child.Type == JTokenType.Property && child.Type != JTokenType.Array &&
                //     child.Type != JTokenType.Object)
                // {
                //     Console.WriteLine("Type: " + child.Type);
                //     Console.WriteLine("Value: " + property.Value);
                //     tempDic.Add(Guid.NewGuid().ToString(), property.Value);
                // } 

                if (property != null)
                {
                    // Console.WriteLine("Child Type: " + child.Type);
                    Console.WriteLine("Value: " + property.Name + " at level: " + level.ToString());
                    // Console.WriteLine("Property Type: " + property.Type);

                    if (property.Value.Type == JTokenType.Array)
                    {
                        var arr = property.Value as JArray;

                        foreach(var item in arr)
                        {
                            var pp2 = item as JProperty;
                            Console.WriteLine("Item: " + pp2.Name);
                        }
                        tempDic.Add(Guid.NewGuid().ToString() + "_arr", property.Value);
                        
                    }
                    else
                    {
                        tempDic.Add(Guid.NewGuid().ToString(), property.Value);
                    }

                }

                level++;
                GetAllProperties(child);
            }

            return tempDic;
        }

        static async Task<HttpResponseMessage> GetResponse()
        {
            var httpClient = new HttpClient();
            httpClient.DefaultRequestHeaders.Accept.Clear();
            httpClient.DefaultRequestHeaders.Authorization =
                new AuthenticationHeaderValue("Bearer", Token);

            var response = await httpClient.GetAsync(
                "https://api.lumenaza.de/v3/prices/?zipcode=30449&consumption=2000&tariff_type=sma_community_strom");

            return response;
        }
    }

    public static class NewtonsoftExtensions
    {
        public static void Rename(this JToken token, string newName)
        {
            if (token == null)
                throw new ArgumentNullException("token", "Cannot rename a null token");

            JProperty property;

            if (token.Type == JTokenType.Property)
            {
                if (token.Parent == null)
                    throw new InvalidOperationException("Cannot rename a property with no parent");

                property = (JProperty)token;
            }
            else
            {
                if (token.Parent == null || token.Parent.Type != JTokenType.Property)
                    throw new InvalidOperationException("This token's parent is not a JProperty; cannot rename");

                property = (JProperty)token.Parent;
            }

            // Note: to avoid triggering a clone of the existing property's value,
            // we need to save a reference to it and then null out property.Value
            // before adding the value to the new JProperty.  
            // Thanks to @dbc for the suggestion.

            var existingValue = property.Value;
            property.Value = null;
            var newProperty = new JProperty(newName, existingValue);
            property.Replace(newProperty);
        }
    }
}